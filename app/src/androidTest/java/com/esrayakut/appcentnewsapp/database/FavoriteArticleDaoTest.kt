package com.esrayakut.appcentnewsapp.database

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.esrayakut.appcentnewsapp.getOrAwaitValue
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class FavoriteArticleDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: FavoriteArticleDatabase
    private lateinit var dao: FavoriteArticleDao


    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            FavoriteArticleDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.favoriteArticleDao()
    }


    @After
    fun teardown() {
        database.close()
    }


    @Test
    fun addFavoriteArticle() = runBlockingTest {
        val article = FavoriteArticle("Esra Yakut", "Lorem ipsum dolor sit amet, consectetur adipisc elidictum eu. Lorem ipsum dolor sit ame...",
            "https://gitlab.com/esrayakut12/appcent-newsapp", "Appcent NewsApp", "2021-04-06T00:00:00Z",
            "Lorem ipsum dolor sit ame...", "https://gitlab.com/esrayakut12/appcent-newsapp")
        dao.addFavoriteArticle(article)

        val allArticleList = dao.getFavoriteArticleList().getOrAwaitValue()

        assertThat(allArticleList).contains(article)
    }


    @Test
    fun deleteFavoriteArticle() = runBlockingTest {
        val title = "Appcent NewsApp"
        val article = FavoriteArticle("Esra Yakut", "Lorem ipsum dolor sit amet, consectetur adipisc elidictum eu. Lorem ipsum dolor sit ame...",
            "https://gitlab.com/esrayakut12/appcent-newsapp", "Appcent NewsApp", "2021-04-06T00:00:00Z",
            "Lorem ipsum dolor sit ame...", "https://gitlab.com/esrayakut12/appcent-newsapp")

        dao.addFavoriteArticle(article)
        dao.deleteFavoriteArticle(title)

        val allArticleList = dao.getFavoriteArticleList().getOrAwaitValue()

        assertThat(allArticleList).doesNotContain(article)
    }

}