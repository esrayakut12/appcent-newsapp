/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.repositories.FavoritesRepository

class FavoritesViewModel(application: Application) : AndroidViewModel(application) {

    private val favoritesRepository = FavoritesRepository(application)


    /**
     * Adding element to the database
     * @param article element to be added
     */
    fun addFavoriteArticle(article: FavoriteArticle) {
        favoritesRepository.addFavoriteArticle(article)
    }


    /**
     * Getting article list from the database
     * @return articles in the database
     */
    fun getFavoriteArticleList() : LiveData<List<FavoriteArticle>?> {
        return favoritesRepository.getFavoriteArticleList()
    }


    /**
     * Getting favorite article from the database
     * @param title the title of the element to be fetched
     * @return article in the database
     */
    fun getFavoriteArticle(title: String) : LiveData<List<FavoriteArticle>?> {
        return favoritesRepository.getFavoriteArticle(title)
    }


    /**
     * Deleting element from the database
     * @param title the title of the element to be deleted
     */
    fun deleteFavoriteArticle(title: String) {
        favoritesRepository.deleteFavoriteArticle(title)
    }

}