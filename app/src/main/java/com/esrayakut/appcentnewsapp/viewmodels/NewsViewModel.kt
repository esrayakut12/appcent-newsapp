/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.repositories.NewsRepository
import com.esrayakut.appcentnewsapp.webresponses.ArticleListResponse

class NewsViewModel(application: Application) : AndroidViewModel(application) {

    private val newsRepository = NewsRepository(application.applicationContext)

    /**
     * Getting the article list from API
     * @param query searched word
     * @param page which page
     * @param apiKey api key
     * @param language in which language
     * @param sortBy how to sort
     * @return LiveData<ArticleListResponse>
     */
    fun getArticleList(query: String?, page: Int?, apiKey: String?, language: String?, sortBy: String?, from: String?, to: String?) : LiveData<ArticleListResponse> {
        return newsRepository.getArticleList(query, page, apiKey, language, sortBy, from, to)
    }


    /**
     * Adding element to the database
     * @param article element to be added
     */
    fun addFavoriteArticle(article: FavoriteArticle) {
        newsRepository.addFavoriteArticle(article)
    }

}