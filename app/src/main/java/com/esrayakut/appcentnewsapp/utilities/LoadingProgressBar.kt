/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.esrayakut.appcentnewsapp.R

class LoadingProgressbar(context: Context?) {

    private val alertAdd: Dialog?

    /**
     * Starting loading progress
     */
    fun startProgress() {
        if (alertAdd != null && !alertAdd.isShowing) {
            alertAdd.show()
        }
    }

    /**
     * Stopping loading progress
     */
    fun stopProgress() {
        if (alertAdd != null && alertAdd.isShowing) {
            alertAdd.dismiss()
        }
    }

    val isShowProgressBar: Boolean
        get() {
            if (alertAdd != null && alertAdd.isShowing) {
                return true
            } else if (alertAdd != null && !alertAdd.isShowing) {
                return false
            }
            return true
        }

    init {
        alertAdd = Dialog(context?.applicationContext!!)
        alertAdd.setContentView(R.layout.loading_progress_bar)
        alertAdd.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertAdd.setCancelable(false)
    }
}