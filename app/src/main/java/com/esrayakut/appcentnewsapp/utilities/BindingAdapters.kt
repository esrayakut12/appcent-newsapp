/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

import android.annotation.SuppressLint
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.esrayakut.appcentnewsapp.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_article.*
import java.lang.Exception
import java.text.SimpleDateFormat

@BindingAdapter("android:imageURL")
fun setImageURL(imageView: ImageView, URL: String?) {
    try {
        imageView.alpha = 0f
        Picasso.get().load(URL).noFade().into(imageView, object : Callback {
            override fun onSuccess() {
                imageView.animate().setDuration(0).alpha(1f).start()
            }
            override fun onError(e: Exception?) {

            }
        })
    } catch (e: Exception) {
        Log.e("in setImageURL", e.stackTrace.toString())
    }
}


@SuppressLint("SimpleDateFormat")
@BindingAdapter("android:calendarText")
fun transformCalendar(textView: TextView, dateText: String?) {
    try {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val date = format.parse(dateText!!)
        val format2 = SimpleDateFormat("yyyy-MM-dd")
        val resultDate = format2.format(date!!)
        textView.text = resultDate
    } catch (e: Exception) {
        Log.e("in transformCalendar", e.stackTrace.toString())
    }
}


@BindingAdapter("android:imageResource")
fun setImageResource(imageView: ImageView, id: Int) {
    imageView.setImageResource(id)
}