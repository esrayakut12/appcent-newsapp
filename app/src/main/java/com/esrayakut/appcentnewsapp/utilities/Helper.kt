/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatDelegate
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.activities.ErrorActivity
import com.esrayakut.appcentnewsapp.utilities.Error
import java.util.*


/**
 * Creating the common purpose of error pages
 * @param context context
 * @param code error code
 * @param iconResource error icon
 * @param description error description
 */
fun showErrorActivity(context: Context, code: String, iconResource: Int, description: String) {
    val intent = Intent(context, ErrorActivity::class.java)
    val bundle = Bundle()
    bundle.putSerializable("error", Error(code, iconResource, description))
    intent.putExtras(bundle)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    context.startActivity(intent)
}


/**
 * Hiding the keyboard
 * @param context context
 * @param view current view
 */
fun hideKeyboardFromFragment(context: Context, view: View) {
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}


/**
 * Changing the app language
 * @param context context
 * @param languageCode language code
 */
fun setLanguage(context: Context, languageCode: String) {
    val locale = Locale(languageCode)
    Locale.setDefault(locale)
    val config = context.resources.configuration
    config.setLocale(locale)
    context.resources.updateConfiguration(config, context.resources.displayMetrics)
}


/**
 * Changing the app theme
 * @param context context
 */
fun prepareTheme(context: Context) {
    val cacheHelper = CacheHelper(context)
    when {
        cacheHelper.fetchTheme() == "light" -> {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            context.setTheme(R.style.Theme_AppcentNewsApp)
        }
        cacheHelper.fetchTheme() == "dark" -> {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            context.setTheme(R.style.Theme_AppcentNewsApp_Night)
        }
    }
}