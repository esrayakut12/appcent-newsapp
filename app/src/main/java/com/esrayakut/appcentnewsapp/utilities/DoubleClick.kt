/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

import android.os.Handler
import android.view.View

/**
 * @URL https://gitlab.com/developerdeveloperdeveloper/androidutilslibrary/-/blob/master/androidutils/src/main/java/com/ddd/androidutils/DoubleClick.kt
 */
open class DoubleClick(
    private val doubleClickListener: DoubleClickListener,
    private var interval: Long = 200L
) : View.OnClickListener {

    private val handler = Handler()
    private var counterClicks = 0
    private var isBusy = false

    override fun onClick(view: View) {
        if (!isBusy) {
            isBusy = true

            counterClicks++
            handler.postDelayed({
                if (counterClicks >= 2) {
                    doubleClickListener.onDoubleClickEvent(view)
                }
                if (counterClicks == 1) {
                    doubleClickListener.onSingleClickEvent(view)
                }

                counterClicks = 0
            }, interval)
            isBusy = false
        }
    }
}
