/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

import android.app.Activity
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.esrayakut.appcentnewsapp.R
import kotlinx.android.synthetic.main.item_custom_toast_success.view.*

class CustomToast {

    companion object {

        /**
         * Preparing customized toast message
         * @param context context
         * @param message message to be printed on the screen
         * @param type success or error message
         */
        fun infoToast(context: Context, message: String, type: String) {
            var layout: View? = null
            if (type == "success") {
                layout = LayoutInflater.from(context).inflate(R.layout.item_custom_toast_success, (context as Activity).findViewById(R.id.toast))
            } else if (type == "error") {
                layout = LayoutInflater.from(context).inflate(R.layout.item_custom_toast_error, (context as Activity).findViewById(R.id.toast))
            }
            layout!!.toast_message.text = message

            val toast = Toast(context.applicationContext)
            toast.duration = Toast.LENGTH_LONG
            toast.setGravity(Gravity.TOP or Gravity.FILL_HORIZONTAL, 0, 20)
            toast.view = layout
            toast.show()
        }

    }

}