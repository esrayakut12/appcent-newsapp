/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

data class SlideShow(
    val imageID: Int,
    val titleID: Int,
    val descriptionID: Int
)


data class QueryModel(
    var currentPage: Int? = 1,
    var totalArticles: Int? = 0,
    var isLoading: Boolean? = false,
    var hasMore: Boolean? = false,
    var search: String? = "all",             // API'de q parametresi zorunlu alan
    var language: String? = null,
    var sortBy: String? = null,
    var from: String? = null,
    var to: String? = null
)


data class Error(
        val errorCode: String,
        val errorIconResource: Int,
        val errorDescription: String
) : java.io.Serializable