/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

import android.view.View
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.webresponses.Article

interface ArticleListener {
    fun onArticleClicked(article: Article)
}


interface FavoriteArticleListener {
    fun onFavoriteArticleClicked(article: FavoriteArticle)
}


interface ArticleDoubleClickListener {
    fun onArticleDoubleClickListener(article: Article)
}


interface DoubleClickListener {
    fun onSingleClickEvent(view: View?)
    fun onDoubleClickEvent(view: View?)
}
