/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

import android.content.Context
import com.esrayakut.appcentnewsapp.R

class CacheHelper(context: Context) {

    private val sharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)

    /***
     * Saving whether the promotional pages are watching or not to cache memory
     */
    fun saveIsWatchSlideShow(flag: Boolean) {
        val editor = sharedPreferences.edit()
        editor.putBoolean("is_watch_slide_show", flag)
        editor.apply()
    }

    /***
     * Fetching whether the promotional pages are watching or not from cache memory
     * @return Boolean
     */
    fun fetchIsWatchSlideShow(): Boolean {
        return sharedPreferences.getBoolean("is_watch_slide_show", false)
    }

    /***
     * Saving preferred language to cache memory
     */
    fun saveLanguage(language: String) {
        val editor = sharedPreferences.edit()
        editor.putString("language", language)
        editor.apply()
    }

    /***
     * Fetching preferred language from cache memory
     * @return String
     */
    fun fetchLanguage(): String {
        return sharedPreferences.getString("language", "")!!
    }

    /***
     * Saving preferred theme to cache memory
     */
    fun saveTheme(theme: String) {
        val editor = sharedPreferences.edit()
        editor.putString("theme", theme)
        editor.apply()
    }

    /***
     * Fetching preferred theme from cache memory
     * @return String
     */
    fun fetchTheme(): String {
        return sharedPreferences.getString("theme", "")!!
    }

}