/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.utilities

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.activities.MainActivity
import com.google.firebase.messaging.RemoteMessage

@Suppress("DEPRECATION")
class FirebaseMessagingService : com.google.firebase.messaging.FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        Log.e("project", "Refreshed token: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e("project", "From: " + remoteMessage.from)

        if (remoteMessage.data.isNotEmpty()) {
            Log.e("project", "Message data payload: " + remoteMessage.data)
            createNotification(remoteMessage)
            val myIntent = Intent("Firebase")
            this.sendBroadcast(myIntent)
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.e("project", "Message Notification Body: " + remoteMessage.notification?.body)
        }

    }


    /**
     * Creating the notification
     * @param remoteMessage message from firebase
     */
    private fun createNotification(remoteMessage: RemoteMessage) {
        val map: Map<String, String> = remoteMessage.data
        val message: String? = remoteMessage.data["message"]

        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("titleNotification", map["title"])
        intent.putExtra("messageNotificaton", message)
        val pIntent = PendingIntent.getActivity(this, 15, intent, PendingIntent.FLAG_ONE_SHOT)
        val channelId = "AppcentNewsApp"
        val n: Notification?
        n = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(this, channelId)
                    .setContentTitle(map["title"])
                    .setContentText(message)
                    .setSmallIcon(R.drawable.background_slider_indicator_inactive)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true)
                    .build()
        } else {
            Notification.Builder(this)
                    .setContentTitle(map["title"])
                    .setContentText(message)
                    .setSmallIcon(R.drawable.background_slider_indicator_inactive)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .build()
        }
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                    channelId,
                    getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH
            )
            channel.lightColor = Color.BLUE
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0, n)
    }
}