/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.fragments

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.activities.ArticleActivity
import com.esrayakut.appcentnewsapp.adapters.ArticleAdapter
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.databinding.FragmentNewsBinding
import com.esrayakut.appcentnewsapp.utilities.*
import com.esrayakut.appcentnewsapp.viewmodels.NewsViewModel
import com.esrayakut.appcentnewsapp.webresponses.Article
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.item_filter_dialog.*
import kotlinx.android.synthetic.main.item_sort_dialog.*
import kotlinx.android.synthetic.main.item_sort_dialog.sortRadioGroup
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NewsFragment: Fragment(), ArticleListener, ArticleDoubleClickListener {

    private lateinit var fragmentNewsBinding: FragmentNewsBinding
    private lateinit var newsViewModel: NewsViewModel
    private lateinit var articleListAdapter: ArticleAdapter

    private var articleList = ArrayList<Article>()
    private var queryModel = QueryModel()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        fragmentNewsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news, container, false)
        return fragmentNewsBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doInitialization()
    }


    /***
     * Connecting adapter for listing articles, adding pagination feature to recyclerview, linking lists of filtering option and defining click listener properties
     * @see getArticleList
     * @see clickListeners
     */
    private fun doInitialization() {
        newsViewModel = ViewModelProvider(this).get(NewsViewModel::class.java)
        val linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        fragmentNewsBinding.articleRecyclerView.layoutManager = linearLayoutManager
        articleListAdapter = ArticleAdapter(requireContext(), articleList, this, this, favoriteHeard)
        fragmentNewsBinding.articleRecyclerView.adapter = articleListAdapter
        fragmentNewsBinding.articleRecyclerView.setHasFixedSize(true)
        fragmentNewsBinding.articleRecyclerView.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun loadMoreItems() {
                queryModel.currentPage = queryModel.currentPage?.plus(1)
                getArticleList()
            }

            override fun isLastPage(): Boolean {
                return articleList.size == queryModel.totalArticles!!
            }

            override fun isLoading(): Boolean {
                return queryModel.isLoading!!
            }

        })

        getArticleList()
        clickListeners()
    }


    /***
     * Getting the article list with API request and setting it to adapter
     */
    private fun getArticleList() {
        toggleLoading()
        queryModel.isLoading = true
        newsViewModel.getArticleList(queryModel.search, queryModel.currentPage, API_KEY, queryModel.language, queryModel.sortBy, queryModel.from, queryModel.to).observe(requireActivity(), {
            toggleLoading()
            if (it != null) {
                queryModel.totalArticles = it.totalResults
                queryModel.isLoading = false
                if (!it.articles.isNullOrEmpty()) {
                    fragmentNewsBinding.isNoData = false
                    val oldCount = articleList.size
                    articleList.addAll(it.articles)
                    articleListAdapter.notifyItemRangeInserted(oldCount, articleList.size)
                } else {
                    fragmentNewsBinding.isNoData = true
                }
            }
        }
        )
    }


    /***
     * Updating the visibility of progress bars
     */
    private fun toggleLoading() {
        if (queryModel.currentPage == 1) {
            if (fragmentNewsBinding.isLoading != null && fragmentNewsBinding.isLoading == 0) {
                fragmentNewsBinding.isLoading = View.GONE
            } else {
                fragmentNewsBinding.isLoading = View.VISIBLE
            }
            fragmentNewsBinding.isLoadingMore = View.GONE
        } else {
            if (fragmentNewsBinding.isLoadingMore != null && fragmentNewsBinding.isLoadingMore == 0) {
                fragmentNewsBinding.isLoadingMore = View.GONE
            } else {
                fragmentNewsBinding.isLoadingMore = View.VISIBLE
            }
        }
    }


    /***
     * Giving click properties to components
     */
    private fun clickListeners() {

        searchArticle.setOnClickListener {
            editSearch.clearFocus()
            hideKeyboardFromFragment(requireContext(), newsMainLayout)
            queryModel = QueryModel()
            if (!editSearch.text.isNullOrBlank())
                queryModel.search = editSearch.text.toString()
            articleList.clear()
            getArticleList()
        }

        clearText.setOnClickListener {
            editSearch.text?.clear()
            editSearch.clearFocus()
            hideKeyboardFromFragment(requireContext(), newsMainLayout)
            queryModel = QueryModel(1, 0, false, false,
                "all", queryModel.language, queryModel.sortBy)
            articleList.clear()
            getArticleList()
        }

        filterDate.setOnClickListener {
            showFilterDatePicker()
        }

        filter.setOnClickListener {
            showFilterDialog()
        }

        sort.setOnClickListener {
            showSortDialog()
        }

    }


    /***
     * Showing which date range to select with dialog picker
     */
    private fun showFilterDatePicker() {
        val builder = MaterialDatePicker.Builder.dateRangePicker()
        val now = Calendar.getInstance()
        builder.setSelection(androidx.core.util.Pair(now.timeInMillis, now.timeInMillis))
        val picker = builder.build()
        picker.show(parentFragmentManager, picker.toString())

        picker.addOnNegativeButtonClickListener {
            picker.dismiss()
        }
        picker.addOnPositiveButtonClickListener {
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            val calendar1 = Calendar.getInstance()
            calendar1.timeInMillis = it.first!!
            val calendar2 = Calendar.getInstance()
            calendar2.timeInMillis = it.second!!

            queryModel = QueryModel(1, 0, false, false,
                queryModel.search, queryModel.language, queryModel.sortBy, formatter.format(calendar1.time), formatter.format(calendar2.time))
            articleList.clear()
            getArticleList()
        }
    }


    /***
     * Showing in which language to filter with dialog
     * @see getArticleList
     */
    private fun showFilterDialog() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.item_filter_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)

        when (queryModel.language) {
            "en" -> dialog.filterRadioGroup.check(dialog.radioEnglish.id)
            "de" -> dialog.filterRadioGroup.check(dialog.radioGerman.id)
            "es" -> dialog.filterRadioGroup.check(dialog.radioSpanish.id)
            "fr" -> dialog.filterRadioGroup.check(dialog.radioFrench.id)
        }

        dialog.show()

        dialog.filterRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            dialog.dismiss()
            when (checkedId) {
                dialog.radioEnglish.id -> queryModel = QueryModel(1, 0, false, false,
                    queryModel.search, "en", queryModel.sortBy)
                dialog.radioGerman.id -> queryModel = QueryModel(1, 0, false, false,
                    queryModel.search, "de", queryModel.sortBy)
                dialog.radioSpanish.id -> queryModel = QueryModel(1, 0, false, false,
                    queryModel.search, "es", queryModel.sortBy)
                dialog.radioFrench.id -> queryModel = QueryModel(1, 0, false, false,
                    queryModel.search, "fr", queryModel.sortBy)
            }
            articleList.clear()
            getArticleList()
        }

        dialog.clearFilter.setOnClickListener {
            dialog.dismiss()
            queryModel = QueryModel(1, 0, false, false, queryModel.search,
                null, queryModel.sortBy)
            articleList.clear()
            getArticleList()
        }
    }


    /***
     * Showing of how to sort with dialog
     * @see getArticleList
     */
    private fun showSortDialog() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.item_sort_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)

        if (queryModel.sortBy == "popularity")
            dialog.sortRadioGroup.check(dialog.radioPopularity.id)
        else
            dialog.sortRadioGroup.check(dialog.radioPublishedAt.id)

        dialog.show()

        dialog.sortRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            dialog.dismiss()
            when (checkedId) {
                dialog.radioPopularity.id -> queryModel = QueryModel(1, 0, false, false,
                    queryModel.search, queryModel.language, "popularity")
                dialog.radioPublishedAt.id -> queryModel = QueryModel(1, 0,  false, false,
                    queryModel.search, queryModel.language, "publishedAt")
            }
            articleList.clear()
            getArticleList()
        }
    }


    /***
     * Saving data to database
     * @param article the data to be saved
     */
    private fun addFavoriteArticle(article: Article) {
        val favoriteArticle = FavoriteArticle(article.author, article.content, article.urlToImage, article.title!!,
            article.publishedAt, article.description, article.url)
        newsViewModel.addFavoriteArticle(favoriteArticle)
    }


    /**
     * What happens when the element of the list is clicked
     * @param article which element of the list
     */
    override fun onArticleClicked(article: Article) {
        val intent = Intent(requireContext(), ArticleActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("article", article)
        intent.putExtras(bundle)
        startActivity(intent)
    }


    /**
     * What happens when the element of the list is double clicked
     * @param article which element of the list
     */
    override fun onArticleDoubleClickListener(article: Article) {
        addFavoriteArticle(article)
        CustomToast.infoToast(requireContext(), getString(R.string.save_favorite), "success")
    }

}