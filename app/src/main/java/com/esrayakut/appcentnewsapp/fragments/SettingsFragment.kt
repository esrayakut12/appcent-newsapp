/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.fragments

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.activities.MainActivity
import com.esrayakut.appcentnewsapp.utilities.CacheHelper
import com.esrayakut.appcentnewsapp.utilities.prepareTheme
import com.esrayakut.appcentnewsapp.utilities.setLanguage
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.item_settings_dialog.*

class SettingsFragment: Fragment() {

    private lateinit var cacheHelper: CacheHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cacheHelper = CacheHelper(requireContext())

        Log.e("language", cacheHelper.fetchLanguage())
        Log.e("theme", cacheHelper.fetchTheme())

        clickListeners()
    }


    /**
     * Giving the ability to click on elements
     * @see showLanguageDialog
     * @see showThemeDialog
     */
    private fun clickListeners() {

        settingsLanguage.setOnClickListener {
            showLanguageDialog()
        }

        settingsTheme.setOnClickListener {
            showThemeDialog()
        }

    }


    /**
     * Representation of which languages can be selected
     * @see setLanguage
     */
    private fun showLanguageDialog() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.item_settings_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)

        dialog.itemTitle.text = getString(R.string.language)
        dialog.radio1.text = getString(R.string.english)
        dialog.radio2.text = getString(R.string.turkish)

        if (cacheHelper.fetchLanguage() == "tr")
            dialog.radioGroup.check(R.id.radio2)
        else
            dialog.radioGroup.check(R.id.radio1)

        dialog.show()

        dialog.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            dialog.dismiss()
            when (checkedId) {
                dialog.radio1.id -> {
                    cacheHelper.saveLanguage("en")
                    setLanguage(requireContext(), "en")
                }
                dialog.radio2.id -> {
                    cacheHelper.saveLanguage("tr")
                    setLanguage(requireContext(), "tr")
                }
            }
            startActivity(Intent(requireContext(), MainActivity::class.java))
            requireActivity().finish()
        }
    }


    /**
     * Representation of which themes can be selected
     */
    private fun showThemeDialog() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.item_settings_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)

        dialog.itemTitle.text = getString(R.string.theme)
        dialog.radio1.text = getString(R.string.light)
        dialog.radio2.text = getString(R.string.dark)

        if (cacheHelper.fetchTheme() == "dark")
            dialog.radioGroup.check(R.id.radio2)
        else
            dialog.radioGroup.check(R.id.radio1)

        dialog.show()

        dialog.radioGroup.setOnCheckedChangeListener { _, checkedId ->
            dialog.dismiss()
            when (checkedId) {
                dialog.radio1.id -> {
                    cacheHelper.saveTheme("light")
                    context?.setTheme(R.style.Theme_AppcentNewsApp_Night)
                }
                dialog.radio2.id -> {
                    cacheHelper.saveTheme("dark")
                    context?.setTheme(R.style.Theme_AppcentNewsApp_Night)
                }
            }
            startActivity(Intent(requireContext(), MainActivity::class.java))
            requireActivity().finish()
        }
    }

}