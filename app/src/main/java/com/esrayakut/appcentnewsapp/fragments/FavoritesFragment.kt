/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.fragments

import android.content.Intent
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.activities.ArticleActivity
import com.esrayakut.appcentnewsapp.adapters.FavoriteArticleAdapter
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.databinding.FragmentFavoritesBinding
import com.esrayakut.appcentnewsapp.utilities.FavoriteArticleListener
import com.esrayakut.appcentnewsapp.utilities.CustomToast
import com.esrayakut.appcentnewsapp.utilities.prepareTheme
import com.esrayakut.appcentnewsapp.viewmodels.FavoritesViewModel
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

class FavoritesFragment: Fragment(), FavoriteArticleListener {

    private lateinit var fragmentFavoritesBinding: FragmentFavoritesBinding
    private lateinit var favoritesViewModel: FavoritesViewModel
    private lateinit var articleListAdapter: FavoriteArticleAdapter

    private var favoriteArticleList = ArrayList<FavoriteArticle>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        fragmentFavoritesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, container, false)
        return fragmentFavoritesBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favoritesViewModel = ViewModelProvider(this).get(FavoritesViewModel::class.java)
        doInitialization()
    }


    /***
     * Connecting adapter for listing articles, adding pagination feature to recyclerview, linking lists of filtering option and defining click listener properties
     * @see getArticleList
     */
    private fun doInitialization() {
        favoritesViewModel = ViewModelProvider(this).get(FavoritesViewModel::class.java)
        val linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        fragmentFavoritesBinding.favoritesRecyclerView.layoutManager = linearLayoutManager
        articleListAdapter = FavoriteArticleAdapter(requireContext(), favoriteArticleList, this)
        fragmentFavoritesBinding.favoritesRecyclerView.adapter = articleListAdapter
        fragmentFavoritesBinding.favoritesRecyclerView.setHasFixedSize(true)
        helper.attachToRecyclerView(fragmentFavoritesBinding.favoritesRecyclerView)

        getArticleList()
    }


    /***
     * Getting the article list with API request and setting it to adapter
     */
    private fun getArticleList() {
        favoritesViewModel.getFavoriteArticleList().observe(requireActivity(), {
            if (it != null && it.isNotEmpty()) {
                fragmentFavoritesBinding.isNoData = false
                val oldCount = favoriteArticleList.size
                favoriteArticleList.clear()
                favoriteArticleList.addAll(it)
                articleListAdapter.notifyItemRangeInserted(oldCount, favoriteArticleList.size)
            } else {
                fragmentFavoritesBinding.isNoData = true
            }
        }
        )
    }


    /***
     * Deleting the requested article from the database
     * @param title the title of the article you want to delete
     */
    private fun deleteFavoriteArticle(title: String) {
        favoritesViewModel.deleteFavoriteArticle(title)
        CustomToast.infoToast(requireContext(), getString(R.string.delete_favorite), "success")
    }


    /**
     * What happens when the element of the list is clicked
     * @param article which element of the list
     */
    override fun onFavoriteArticleClicked(article: FavoriteArticle) {
        val intent = Intent(requireContext(), ArticleActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("article", article)
        intent.putExtras(bundle)
        startActivity(intent)
    }


    private var helper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        override fun onMove(@NonNull recyclerView: RecyclerView, @NonNull viewHolder:RecyclerView.ViewHolder, @NonNull target:RecyclerView.ViewHolder) : Boolean {
            return false
        }
        override fun onSwiped(@NonNull viewHolder:RecyclerView.ViewHolder, direction:Int) {
            val position = viewHolder.adapterPosition
            if (direction == ItemTouchHelper.RIGHT) {
                deleteFavoriteArticle(favoriteArticleList[position].title)
                articleListAdapter.removeItem(position)
            }
        }
        override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
            RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                .addBackgroundColor(ContextCompat.getColor(requireContext(), R.color.red))
                .addActionIcon(R.drawable.ic_delete)
                .create()
                .decorate()
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
    })

}