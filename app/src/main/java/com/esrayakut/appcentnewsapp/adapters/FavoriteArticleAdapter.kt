/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.databinding.ItemContainerFavoriteArticleBinding
import com.esrayakut.appcentnewsapp.utilities.FavoriteArticleListener

class FavoriteArticleAdapter(private val context: Context, private var articleList: ArrayList<FavoriteArticle>, private val articleListener: FavoriteArticleListener) : RecyclerView.Adapter<FavoriteArticleAdapter.ArticleViewHolder>() {

    private var layoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val articleBinding = DataBindingUtil.inflate<ItemContainerFavoriteArticleBinding>(layoutInflater!!, R.layout.item_container_favorite_article, parent, false)
        return ArticleViewHolder(articleBinding)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.bindArticle(articleList[position])
        setFadeAnimation(holder.itemView)
    }

    override fun getItemCount(): Int {
        return articleList.size
    }


    /***
     * Deleting the element from the list and update the adapter
     * @param index the index of the element to be deleted
     */
    fun removeItem(index: Int) {
        articleList.removeAt(index)
        notifyItemRemoved(index)
        notifyItemRangeChanged(index, itemCount)
    }


    /***
     * Creating and starting the animation
     * @param view view of project list elements
     */
    private fun setFadeAnimation(view: View) {
        val anim = AnimationUtils.loadAnimation(context, R.anim.anim_recyclerview)
        anim.duration = 500
        view.startAnimation(anim)
    }


    inner class ArticleViewHolder constructor(private val itemContainerArticleBinding: ItemContainerFavoriteArticleBinding) : RecyclerView.ViewHolder(itemContainerArticleBinding.root) {

        fun bindArticle(article: FavoriteArticle) {
            itemContainerArticleBinding.article = article
            itemContainerArticleBinding.executePendingBindings()
            itemContainerArticleBinding.root.setOnClickListener {
                articleListener.onFavoriteArticleClicked(article)
            }
        }

    }

}