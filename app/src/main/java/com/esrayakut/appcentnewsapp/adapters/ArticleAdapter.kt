/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.adapters

import android.content.Context
import android.graphics.drawable.AnimatedVectorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.databinding.ItemContainerArticleBinding
import com.esrayakut.appcentnewsapp.utilities.*
import com.esrayakut.appcentnewsapp.webresponses.Article
import com.google.android.material.imageview.ShapeableImageView

class ArticleAdapter(private val context: Context, private var articleList: List<Article>, private val articleListener: ArticleListener, private val articleDoubleClickListener: ArticleDoubleClickListener, val heardImage: ShapeableImageView) : RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>() {

    private var layoutInflater: LayoutInflater? = null
    private lateinit var avd: AnimatedVectorDrawableCompat
    private lateinit var avd2: AnimatedVectorDrawable

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val articleBinding = DataBindingUtil.inflate<ItemContainerArticleBinding>(layoutInflater!!, R.layout.item_container_article, parent, false)
        return ArticleViewHolder(articleBinding)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.bindArticle(articleList[position])
        setFadeAnimation(holder.itemView)
    }

    override fun getItemCount(): Int {
        return articleList.size
    }


    /***
     * Creating and starting the animation
     * @param view view of article list elements
     */
    private fun setFadeAnimation(view: View) {
        val anim = AnimationUtils.loadAnimation(context, R.anim.anim_recyclerview)
        anim.duration = 500
        view.startAnimation(anim)
    }


    inner class ArticleViewHolder constructor(private val itemContainerArticleBinding: ItemContainerArticleBinding) : RecyclerView.ViewHolder(itemContainerArticleBinding.root) {

        fun bindArticle(article: Article) {
            itemContainerArticleBinding.article = article
            itemContainerArticleBinding.executePendingBindings()

            val doubleClick = DoubleClick(object : DoubleClickListener {
                override fun onSingleClickEvent(view: View?) {
                    articleListener.onArticleClicked(article)
                }

                override fun onDoubleClickEvent(view: View?) {
                    heardImage.alpha = 0.7f
                    if (heardImage.drawable is AnimatedVectorDrawableCompat) {
                        avd = heardImage.drawable as AnimatedVectorDrawableCompat
                        avd.start()
                    } else if (heardImage.drawable is AnimatedVectorDrawable) {
                        avd2 = heardImage.drawable as AnimatedVectorDrawable
                        avd2.start()
                    }
                    articleDoubleClickListener.onArticleDoubleClickListener(article)
                }

            })

            itemContainerArticleBinding.root.setOnClickListener(doubleClick)
        }

    }

}