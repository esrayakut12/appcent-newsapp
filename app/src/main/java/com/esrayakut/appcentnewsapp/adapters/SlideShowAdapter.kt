/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.utilities.SlideShow
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.textview.MaterialTextView

class SlideShowAdapter(private val slideShowList: List<SlideShow>) : PagerAdapter() {

    private lateinit var layoutInflater: LayoutInflater

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater.inflate(R.layout.item_slide_show, container, false)
        view.findViewById<ShapeableImageView>(R.id.itemSlideShowImage).setImageResource(slideShowList[position].imageID)
        view.findViewById<MaterialTextView>(R.id.itemSlideShowTitle).setText(slideShowList[position].titleID)
        view.findViewById<MaterialTextView>(R.id.itemSlideShowDescription).setText(slideShowList[position].descriptionID)
        container.addView(view)
        return view
    }

    override fun getCount(): Int {
        return slideShowList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val view: View = `object` as View
        container.removeView(view)
    }

}