/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.webresponses

import kotlin.collections.ArrayList

data class ArticleListResponse(
    var status: String?,
    var articles: ArrayList<Article>,
    var totalResults: Int?
) : java.io.Serializable


data class Article(
    var author: String?,
    var source: Source?,
    var content: String?,
    var urlToImage: String?,
    var title: String?,
    var publishedAt: String?,
    var description: String?,
    var url: String?
) : java.io.Serializable


data class Source(
    var id: String?,
    var name: String?
) : java.io.Serializable