/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface FavoriteArticleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavoriteArticle(article: FavoriteArticle)

    @Query("SELECT * FROM favorite_article_table")
    fun getFavoriteArticleList() : LiveData<List<FavoriteArticle>?>

    @Query("SELECT * FROM favorite_article_table WHERE title == :title")
    fun getFavoriteArticle(title: String) : LiveData<List<FavoriteArticle>?>

    @Query("DELETE FROM favorite_article_table WHERE title == :title")
    fun deleteFavoriteArticle(title: String)

}