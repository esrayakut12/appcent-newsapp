/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.esrayakut.appcentnewsapp.utilities.DATABASE

@Database(entities = [FavoriteArticle::class], version = 1, exportSchema = false)
abstract class FavoriteArticleDatabase : RoomDatabase() {

    abstract fun favoriteArticleDao() : FavoriteArticleDao

    companion object {

        @Volatile
        private var INSTANCE: FavoriteArticleDatabase? = null

        /**
         * Creating the room database
         * @param context context
         * @return room database
         */
        fun getDatabase(context: Context) : FavoriteArticleDatabase {

            if (INSTANCE != null)
                return INSTANCE!!

            synchronized(this) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, FavoriteArticleDatabase::class.java, DATABASE)
                    .fallbackToDestructiveMigration()
                    .build()
                return INSTANCE!!
            }
        }
    }

}