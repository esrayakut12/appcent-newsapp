/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.esrayakut.appcentnewsapp.utilities.FAVORITE_ARTICLE_TABLE

@Entity(tableName = FAVORITE_ARTICLE_TABLE)
data class FavoriteArticle(
    val author: String?,
    val content: String?,
    val urlToImage: String?,
    @PrimaryKey val title: String,
    var publishedAt: String?,
    val description: String?,
    val url: String?
) : java.io.Serializable