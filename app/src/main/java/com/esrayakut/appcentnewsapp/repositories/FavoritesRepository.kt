/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.repositories

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.database.FavoriteArticleDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class FavoritesRepository(application: Application) {

    private var database = FavoriteArticleDatabase.getDatabase(application)
    private val favoriteArticleDao = database.favoriteArticleDao()


    /**
     * Adding element to the database
     * @param article element to be added
     */
    fun addFavoriteArticle(article: FavoriteArticle) {
        CoroutineScope(IO).launch {
            database.favoriteArticleDao().addFavoriteArticle(article)
        }
    }


    /**
     * Getting article list from the database
     * @return articles in the database
     */
    fun getFavoriteArticleList() : LiveData<List<FavoriteArticle>?> {
        return favoriteArticleDao.getFavoriteArticleList()
    }


    /**
     * Getting favorite article from the database
     * @param title the title of the element to be fetched
     * @return article in the database
     */
    fun getFavoriteArticle(title: String) : LiveData<List<FavoriteArticle>?> {
        return favoriteArticleDao.getFavoriteArticle(title)
    }


    /**
     * Deleting element from the database
     * @param title the title of the element to be deleted
     */
    fun deleteFavoriteArticle(title: String) {
        CoroutineScope(IO).launch {
            favoriteArticleDao.deleteFavoriteArticle(title)
        }
    }

}