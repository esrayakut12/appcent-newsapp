/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.repositories

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.database.FavoriteArticleDatabase
import com.esrayakut.appcentnewsapp.network.ApiClient
import com.esrayakut.appcentnewsapp.network.CustomCallBack
import com.esrayakut.appcentnewsapp.webresponses.ArticleListResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response

class NewsRepository(private val context: Context) {

    private var database = FavoriteArticleDatabase.getDatabase(context.applicationContext)
    private val favoriteArticleDao = database.favoriteArticleDao()

    /***
     * Getting the article list from the API
     * @param query searched word
     * @param page which page
     * @param apiKey api key
     * @param language in which language
     * @param sortBy how to sort
     * @return LiveData<ArticleListResponse>
     */
    fun getArticleList(query: String?, page: Int?, apiKey: String?, language: String?, sortBy: String?, from: String?, to: String?) : LiveData<ArticleListResponse> {

        val data = MutableLiveData<ArticleListResponse>()

        ApiClient(context).getNewsAPI().getArticleList(query, page, apiKey, language, sortBy, from, to)
            .enqueue(object : CustomCallBack<ArticleListResponse?>(context, false) {
                override fun onResponse(call: Call<ArticleListResponse?>, response: Response<ArticleListResponse?>) {
                    super.onResponse(call, response)
                    data.value = response.body()
                }

                override fun onFailure(call: Call<ArticleListResponse?>, t: Throwable) {
                    super.onFailure(call, t)
                    data.value = null
                }
            })

        return data

    }


    /**
     * Adding element to the database
     * @param article element to be added
     */
    fun addFavoriteArticle(article: FavoriteArticle) {
        CoroutineScope(Dispatchers.IO).launch {
            database.favoriteArticleDao().addFavoriteArticle(article)
        }
    }

}