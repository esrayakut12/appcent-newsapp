/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.network

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response

class ApiInterceptor(private var context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val rqBuilder = chain.request().newBuilder()
        return chain.proceed(rqBuilder.build())

    }

}