/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.network

import android.content.Context
import com.esrayakut.appcentnewsapp.R
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient(private val context: Context) {

    private val baseUrl: String = "${context.getString(R.string.base_url)}${context.getString(R.string.version)}"

    /***
     * Creating interceptor, okHttpClient and retrofit
     * @return Retrofit
     */
    private fun getClient(): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val okHttpClient: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(ApiInterceptor(context))
            .addInterceptor(NetworkConnectionInterceptor(context))
            .build()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    /***
     * API links related to news
     * @return NewsEndpoints
     */
    fun getNewsAPI(): NewsEndpoints {
        return getClient().create(NewsEndpoints::class.java)
    }

}