/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.network

import com.esrayakut.appcentnewsapp.webresponses.ArticleListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsEndpoints {

    @GET("everything")
    fun getArticleList(
        @Query("q") search: String?,
        @Query("page") page: Int?,
        @Query("apiKey") apiKey: String?,
        @Query("language") language: String?,
        @Query("sortBy") sortBy: String?,
        @Query("from") from: String?,
        @Query("to") to: String?
    ) : Call<ArticleListResponse>

}