/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.network

import android.content.Context
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.utilities.LoadingProgressbar
import com.esrayakut.appcentnewsapp.utilities.showErrorActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class CustomCallBack<T>(private val context: Context, loading: Boolean) : Callback<T> {

    private val tag: String = CustomCallBack::class.java.simpleName
    private val loadingDialog: LoadingProgressbar = LoadingProgressbar(context)

    init {
        if (loading) {
            loadingDialog.startProgress()
        } else {
            loadingDialog.stopProgress()
        }
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        loadingDialog.stopProgress()
        when {
            response.code() == 500 -> {
                showErrorActivity(context, "500",  R.drawable.ic_error, context.getString(R.string.error_500))
            }
            response.code() == 404 -> {
                showErrorActivity(context, "404",  R.drawable.ic_not_page, context.getString(R.string.error_404))
            }
            response.code() == 403 -> {
                showErrorActivity(context, "403",  R.drawable.ic_error, context.getString(R.string.error_403))
            }
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {

        if (t is NetworkConnectionInterceptor.NoConnectivityException) {
            showErrorActivity(context, "not internet",  R.drawable.ic_not_internet, context.getString(R.string.not_internet))
        }

        t.printStackTrace()
        loadingDialog.stopProgress()
    }

}