/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.utilities.CacheHelper
import com.esrayakut.appcentnewsapp.utilities.prepareTheme
import com.esrayakut.appcentnewsapp.utilities.setLanguage

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        prepareTheme(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val cacheHelper = CacheHelper(this)
        setLanguage(this, cacheHelper.fetchLanguage())

        Handler(Looper.getMainLooper()).postDelayed({
            finish()
            if (cacheHelper.fetchIsWatchSlideShow()) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                startActivity(Intent(this, SlideShowActivity::class.java))
            }
        }, 3000)

    }

}