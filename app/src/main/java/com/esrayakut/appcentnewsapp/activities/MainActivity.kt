/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.activities

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.utilities.prepareTheme
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var navController: NavController

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.bottomNews -> {
                navController.navigate(R.id.fragment_bottom_news)
                bottomNavigation.menu.getItem(0).isChecked = true
            }

            R.id.bottomFavorites -> {
                navController.navigate(R.id.fragment_bottom_favorites)
                bottomNavigation.menu.getItem(1).isChecked = true
            }

            R.id.bottomSettings -> {
                navController.navigate(R.id.fragment_bottom_settings)
                bottomNavigation.menu.getItem(2).isChecked = true
            }
        }
        true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        prepareTheme(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = (supportFragmentManager.findFragmentById(R.id.mainFragmentContainer) as NavHostFragment).navController
        bottomNavigation.menu.getItem(0).isChecked = true
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }


    /***
     * Finding out which index is selected in bottom navigation
     */
    private fun findBottomNavigationItemSelected(): Int {
        for (i in 0 until bottomNavigation.menu.size()) {
            if (bottomNavigation.menu.getItem(i).isChecked) {
                return i
            }
        }
        return -1
    }


    override fun onBackPressed() {
        bottomNavigation.visibility = View.VISIBLE
        when {
            findBottomNavigationItemSelected() != 0 -> {
                navController.navigate(R.id.fragment_bottom_news, null, NavOptions.Builder().setPopUpTo(R.id.fragment_bottom_news, true).build())
                bottomNavigation.menu.getItem(0).isChecked = true
            }
            else -> {
                finish()
            }
        }
    }

}