/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.viewpager.widget.ViewPager
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.adapters.SlideShowAdapter
import com.esrayakut.appcentnewsapp.utilities.CacheHelper
import com.esrayakut.appcentnewsapp.utilities.SlideShow
import com.esrayakut.appcentnewsapp.utilities.ZoomOutPageTransformer
import com.esrayakut.appcentnewsapp.utilities.prepareTheme
import kotlinx.android.synthetic.main.activity_slide_show.*

class SlideShowActivity : BaseActivity(), ViewPager.OnPageChangeListener {

    private lateinit var cacheHelper: CacheHelper
    private var lastPage = false

    private val slideShowAdapter = SlideShowAdapter(
        listOf(
            SlideShow(
                R.drawable.ic_heart,
                R.string.slide1_title,
                R.string.slide1_description
            ),
            SlideShow(
                R.drawable.ic_delete2,
                R.string.slide2_title,
                R.string.slide2_description
            ),
            SlideShow(
                R.drawable.ic_settings,
                R.string.slide3_title,
                R.string.slide3_description
            )
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        prepareTheme(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slide_show)

        cacheHelper = CacheHelper(this)

        slideShowViewpager.adapter = slideShowAdapter
        slideShowViewpager.addOnPageChangeListener(this)
        slideShowViewpager.setPageTransformer(true, ZoomOutPageTransformer())

        setupIndicators()
        setCurrentIndicator(0)

        slideShowNextButton.setOnClickListener {
            cacheHelper.saveIsWatchSlideShow(true)
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }


    /***
     * Creating indicators
     */
    private fun setupIndicators() {
        val indicators = arrayOfNulls<ImageView>(slideShowAdapter.count)
        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.setMargins(8, 0, 8, 0)

        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i].apply {
                this?.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.background_slider_indicator_inactive))
                this?.layoutParams = layoutParams
            }
            slideShowIndicators.addView(indicators[i])
        }
    }


    /***
     * Update current indicator
     * @param index current indicator's index
     */
    private fun setCurrentIndicator(index: Int) {
        val childCount = slideShowIndicators.childCount
        for (i in 0 until childCount) {
            val imageView = slideShowIndicators[i] as ImageView
            if (i == index) {
                imageView.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.background_slider_indicator_active))
            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.background_slider_indicator_inactive))
            }
        }
    }


    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        setCurrentIndicator(position)
        if (position == 2)
            lastPage = true

        if (lastPage)
            slideShowNextButton.visibility = View.VISIBLE
        else
            slideShowNextButton.visibility = View.INVISIBLE
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

}