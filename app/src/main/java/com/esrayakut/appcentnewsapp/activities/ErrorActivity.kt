/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.databinding.ActivityErrorBinding
import com.esrayakut.appcentnewsapp.utilities.prepareTheme
import com.esrayakut.appcentnewsapp.utilities.Error

class ErrorActivity : AppCompatActivity() {

    private lateinit var activityErrorBinding: ActivityErrorBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        prepareTheme(this)
        super.onCreate(savedInstanceState)
        activityErrorBinding = DataBindingUtil.setContentView(this, R.layout.activity_error)
        doInitialization()
    }


    /***
     * Embedding the error model that comes with intent
     */
    private fun doInitialization() {
        val error = intent.getSerializableExtra("error") as Error
        activityErrorBinding.error = error

        activityErrorBinding.errorTitleBack.setOnClickListener {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

}