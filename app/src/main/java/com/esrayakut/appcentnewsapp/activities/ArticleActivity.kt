/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.database.FavoriteArticle
import com.esrayakut.appcentnewsapp.databinding.ActivityArticleBinding
import com.esrayakut.appcentnewsapp.utilities.CustomToast
import com.esrayakut.appcentnewsapp.utilities.prepareTheme
import com.esrayakut.appcentnewsapp.viewmodels.FavoritesViewModel
import com.esrayakut.appcentnewsapp.webresponses.Article
import kotlinx.android.synthetic.main.activity_article.*

class ArticleActivity : AppCompatActivity() {

    private lateinit var activityArticleBinding: ActivityArticleBinding
    private lateinit var favoritesViewModel: FavoritesViewModel
    private var isAdded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        prepareTheme(this)
        super.onCreate(savedInstanceState)
        activityArticleBinding = DataBindingUtil.setContentView(this, R.layout.activity_article)
        favoritesViewModel = ViewModelProvider(this).get(FavoritesViewModel::class.java)
        doInitialization()
    }


    /***
     * Getting data from intent, integrating it into the design and creating the listener of the button for the website
     */
    private fun doInitialization() {
        var article: Any? = null
        if (intent.getSerializableExtra("article") is Article) {
            article = intent.getSerializableExtra("article") as Article
        } else if (intent.getSerializableExtra("article") is FavoriteArticle) {
            val favoriteArticle = intent.getSerializableExtra("article") as FavoriteArticle
            article = Article(favoriteArticle.author, null, favoriteArticle.content,
                    favoriteArticle.urlToImage, favoriteArticle.title, favoriteArticle.publishedAt,
                    favoriteArticle.description, favoriteArticle.url)
        }
        activityArticleBinding.article = article as Article

        favoritesViewModel.getFavoriteArticle(article.title!!).observe(this, {
            if (!it.isNullOrEmpty()) {
                isAdded = true
                heart.setImageResource(R.drawable.ic_heart)
            } else {
                isAdded = false
                heart.setImageResource(R.drawable.ic_add)
            }
        })

        articleBack.setOnClickListener {
            onBackPressed()
        }

        articleShare.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_url))
            intent.putExtra(Intent.EXTRA_TEXT, article.url)
            startActivity(Intent.createChooser(intent, getString(R.string.share_url)))
        }

        articleAddFavorite.setOnClickListener {
            if (isAdded) {
                heart.setImageResource(R.drawable.ic_add)
                deleteFavoriteArticle(article.title!!)
            } else {
                heart.setImageResource(R.drawable.ic_heart)
                addFavoriteArticle(article)
            }
            isAdded = !isAdded
        }

        goWebsite.setOnClickListener {
            val intent = Intent(this, WebActivity::class.java)
            intent.putExtra("url", article.url)
            startActivity(intent)
        }
    }


    /***
     * Saving data to database
     * @param article the data to be saved
     */
    private fun addFavoriteArticle(article: Article) {
        val favoriteArticle = FavoriteArticle(article.author, article.content, article.urlToImage, article.title!!, article.publishedAt, article.description, article.url)
        favoritesViewModel.addFavoriteArticle(favoriteArticle)
        CustomToast.infoToast(this, getString(R.string.save_favorite), "success")
    }


    /***
     * Deleting data from database
     * @param title the title to be deleted
     */
    private fun deleteFavoriteArticle(title: String) {
        favoritesViewModel.deleteFavoriteArticle(title)
        CustomToast.infoToast(this, getString(R.string.delete_favorite), "success")
    }

}