/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.esrayakut.appcentnewsapp.utilities.CacheHelper
import com.esrayakut.appcentnewsapp.utilities.ContextUtils
import java.util.*

open class BaseActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context?) {
        val cacheHelper = CacheHelper(newBase!!)
        val localeToSwitchTo = Locale(cacheHelper.fetchLanguage())
        val localeUpdatedContext = ContextUtils.updateLocale(newBase, localeToSwitchTo)
        super.attachBaseContext(localeUpdatedContext)
    }

}