/***
 * @author Esra Yakut
 * @since 1.0
 */
package com.esrayakut.appcentnewsapp.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebViewClient
import com.esrayakut.appcentnewsapp.R
import com.esrayakut.appcentnewsapp.utilities.prepareTheme
import kotlinx.android.synthetic.main.activity_web.*

class WebActivity : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        prepareTheme(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        prepareTheme(this)

        val url = intent.getStringExtra("url")

        webview.settings.javaScriptEnabled = true
        webview.webViewClient = WebViewClient()

        if (url != null)
            webview.loadUrl(url)

        errorTitleBack.setOnClickListener {
            onBackPressed()
        }
    }

}