# Appcent NewsApp

## Description

Search through millions of articles from over 75,000 large and small news sources and blogs.

### Features

- Use MVVM architecture and data binding
- A simple introduction page with animation
- Implement infinite scroll
- Implement a search project by name, filter by date and language, and sort by given options
- Use of animation in listing and save to favorites with double click in news page
- Delete element by swiping right
- Design with collapsing toolbar layout
- Share link
- Unit test for room database methods
- Firebase notification connection
- Adding Firebase Crashlytics and Performance features
- Language option (English and Turkish)
- Theme option (Light and Dark)

### Dependencies

Android operatation system, minimum sdk version 21

### Authors

Esra YAKUT
@https://gitlab.com/esrayakut12

### Version History

- 1.0
-- Initial Release

### Help

To send a notification from Firebase:
https://console.firebase.google.com/u/2/project/appcent-newsapp/notification

APK link:
https://gitlab.com/esrayakut12/appcent-newsapp/-/blob/master/app/build/outputs/apk/debug/app-debug.apk
